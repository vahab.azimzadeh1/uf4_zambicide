package humanoide;

import java.util.ArrayList;
import arma.Arma;


public class Jugador extends Humanoide {
	public ArrayList<Arma> armaJugador = new ArrayList<Arma>();
	private Arma arma;
	private boolean ataqueEspecial = true;
	
	public Jugador(String nombreJugador, int salud, int saludMaxima, Arma daga) {
		super(nombreJugador, salud, saludMaxima);
		armaJugador.add(daga);
		this.arma = daga;
		
	}
	
	public void setArma(Arma arma) {
		this.arma = arma;
	}
	public Arma getArma() {
		return arma;
	}
	
	
	@Override
	public String toString() {                                                                     
		return "JUGADOR: " + getNombre() + " S:" + getSalud() + " Arma[" + arma.getNombre() + " DANO:" + arma.getDano() + " DIST:" + arma.getAlcance() + " ACIER:" + arma.getAcierto() + "]";
	}

	public boolean getAtaqueEspecial() {
		return ataqueEspecial;
	} 

	public void setAtaqueEspecial(boolean ataqueEspecial) {
		this.ataqueEspecial = ataqueEspecial;
	}
}
	
	
	
	
	
	
	
	
