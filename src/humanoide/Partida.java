package humanoide;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import arma.Arma;
import principal.Zombicide;
import zombie.Zombie;
import zombie.Caminante;
import zombie.Corredor;
import zombie.Gordo;

public class Partida {
	private ArrayList<Jugador> jugadoresSeleccionada = new ArrayList<Jugador>();
	private ArrayList<Zombie> allZombies = new ArrayList<Zombie>();
	private ArrayList<Zombie> zombiesSeleccionada = new ArrayList<Zombie>();
	private Scanner scanner = new Scanner(System.in);
	private Random rand = new Random();
	private int ronda = 0;
	private int nivel;
	
	public Partida(ArrayList<Jugador> jugadoresSeleccionada, int numeroDePersonaje) {
		this.jugadoresSeleccionada = jugadoresSeleccionada;
		this.nivel = numeroDePersonaje;
		generarZombie();
		partida();
	}
	
	private void partida() {
		zombieAleatorio();
		menuDeAccion();
	}
	
	// flujo principal del juego. Permite que cada jugador tome una acción cada ronda y luego se verifica si se puede continuar con el juego. Si aun quedan
	// zombies y jugadores se llama de nuevo a menuDeAccion() para iniciar otra ronda. Al final de cada nivel se cura a los jugadores y se devuelve la habilidad especial
	private void menuDeAccion() {
		System.out.println("         |----- NIVEL: " + nivel + " - " + ronda + " -----|");
		if(ronda != 0) {
			System.out.print("         ==| ");
			for(int i = 0; i < zombiesSeleccionada.size(); i++) {
				System.out.print(zombiesSeleccionada.get(i).getNombre() + " ");
			}
			System.out.println("|==");
		}
		for(int i = 0; i < jugadoresSeleccionada.size(); i++) {
			while(true) {
				System.out.println(turnoJugador(i));
				System.out.println("1- Atacar\n"
						+ "2- Habilidad Especial\n"
						+ "3- Buscar\n"
						+ "4- Cambiar Arma\n"
						+ "0- Pasar");
				int opcion = scanner.nextInt();
			
				if(opcion == 1) {
					atacar(i);
					break;
				}
				else if(opcion == 2) {
					boolean check = habilidadEspecial(i);
					if(check)
						break;
					else
						continue;
					
				}
				else if(opcion == 3) {
					buscar(i);
				}
				else if(opcion == 4) {
					cambiarArma(i);
				}
				else if(opcion == 0) {
					break;
				}
				else{
					System.out.println("Opción no válida!!! Selecciona una opción valida.");
				}
				System.out.println("");
			}
			if(zombiesSeleccionada.size() == 0) {
				System.out.println("Has ganado este nivel \n");
				nivel++;
				ronda = 0;
				curarJugadores();
				setAtaqueEspecialTrue();
				partida();
			}
		}
		ataqueZombie();
		ronda++;
		if(jugadoresSeleccionada.size() == 0) {
			System.out.println("\n   Los jugadores han muerto. \n   <<Los zombies ganan>> \n\n"
					+ "    Quieres volver a jugar?\n"
					+ "    1- Sí\n"
					+ "    0- No");
			while(true) {
				int opcionn = scanner.nextInt();
				if(opcionn == 1) {
					jugadoresSeleccionada.clear();
					zombiesSeleccionada.clear();
					Zombicide.limpiezaJugadores();
				}
				else if (opcionn == 0) {
					System.out.println("\n     HASTA LUEGO (◠‿◠) ");
					System.exit(0);
				}
				else {
					System.out.println("\n    -----Opción no válida!!! Selecciona una opción valida-----\n");
				}
			}
		}
		else if(zombiesSeleccionada.size() != 0) {
			menuDeAccion();
		}
	}
	
	// cura a todos los jugadores vivos luego de acabar un nivel
	private void curarJugadores() {
		for(int i = 0; i < jugadoresSeleccionada.size(); i++) {
			jugadoresSeleccionada.get(i).curar();
		}
	}
	
	// devuelve a los jugadores el ataque especial para el nuevo nivel
	private void setAtaqueEspecialTrue() {
		for(int i = 0; i < jugadoresSeleccionada.size(); i++) {
			jugadoresSeleccionada.get(i).setAtaqueEspecial(true);
		}
	}
	
	// Proceso de ataque de zombies a jugadores: reduce la vida de los jugadores o los elimina. Se muestra por pantalla que jugadores son afectados y si mueren
	private void ataqueZombie() {
		for(int i = 0; i < zombiesSeleccionada.size(); i++) {
			for(int j = 0; j < zombiesSeleccionada.get(i).getMovimiento() ; j++) {
				int randJugador = rand.nextInt(jugadoresSeleccionada.size());
				jugadoresSeleccionada.get(randJugador).salud = jugadoresSeleccionada.get(randJugador).getSalud() - zombiesSeleccionada.get(i).getDano();       
				System.out.println(zombiesSeleccionada.get(i).getNombre() + " ha atacado a " + jugadoresSeleccionada.get(randJugador).getNombre() );
				if(jugadoresSeleccionada.get(randJugador).salud == 0) {
					System.out.println("El jugador " + jugadoresSeleccionada.get(randJugador).getNombre() + " muerto");
					jugadoresSeleccionada.remove(randJugador);
					if(jugadoresSeleccionada.size() == 0) {
						return;
					}
				}
			}
		}
		System.out.println("");
	}
	
	// Proceso de ataque de jugadores a zombies, muestra mensajes dependiendo de si el ataque falla o no, o si es evitado por el zombie, o si el zombie ha muerto
	private void atacar(int i) {
		for(int j = 0; j < jugadoresSeleccionada.get(i).getArma().getAlcance() ; j++) {
			int randZombi = rand.nextInt(zombiesSeleccionada.size());
			if(jugadoresSeleccionada.get(i).getArma().getDano() >= zombiesSeleccionada.get(randZombi).getSalud()) {  
				int rand_1A6 = rand.nextInt(6) + 1; 
				if(rand_1A6 >= jugadoresSeleccionada.get(i).getArma().getAcierto()) {
					System.out.print("\u001B[31m");
					System.out.println(zombiesSeleccionada.get(randZombi).getNombre() + " muerto");
					System.out.print("\u001B[0m");
					int tipoDeZombie = zombiesSeleccionada.get(randZombi).getTipo();
					zombiesSeleccionada.remove(randZombi);
					initZombie(tipoDeZombie);
					if(zombiesSeleccionada.size() == 0) {
						return;
					}
				}
				else {
					System.out.print("\u001B[31m");
					System.out.println("Ha fallado tu ataque con un " + rand_1A6);
					System.out.print("\u001B[0m");
				}
			}
			else {
				System.out.print("\u001B[31m");
				System.out.println(zombiesSeleccionada.get(randZombi).getNombre() + " ha evitado el ataque");
				System.out.print("\u001B[0m");
			}
		}
		System.out.println("");
	}
	
	// controla la activación de habilidades especiales de los jugadores según el arma que tienen, con diferentes efectos segun el tipo
	private boolean habilidadEspecial(int i) {
		if(jugadoresSeleccionada.get(i).getAtaqueEspecial()) {
			jugadoresSeleccionada.get(i).setAtaqueEspecial(false);
			String nombreArma = jugadoresSeleccionada.get(i).getArma().getNombre();
			if(nombreArma == "hacha") {
				for(int j = 0; j < zombiesSeleccionada.size(); j++) {
					if(zombiesSeleccionada.get(j).getNombre() == "gordo") {
						zombiesSeleccionada.remove(j);
						System.out.println("\n  <<ataque especial activa>>\n");
					}
				}
				return true;
			}
			else if(nombreArma == "hechizo") {
				int countHechizo = 0;
				for(int j = 0; j < zombiesSeleccionada.size(); j++) {
					if(zombiesSeleccionada.get(j).getNombre() == "caminante") {
						zombiesSeleccionada.remove(j);
						System.out.println("\n  <<ataque especial activa>>\n");
						countHechizo++;
						j--;
						if(countHechizo == 2) {
							return true;
						}
					}
				}
				return true;
			}
			else if(nombreArma == "espada") {
				int countEspada = 0;
				for(int j = 0; j < zombiesSeleccionada.size(); j++) {
					int randEspada = rand.nextInt(zombiesSeleccionada.size());
					zombiesSeleccionada.remove(randEspada);
					System.out.println("\n  <<ataque especial activa>>\n");
					countEspada++;
					j--;
					if(countEspada == 2) {
						return true;
					}
				}
				return true;
			}
			else if(nombreArma == "arco") {
				for(int j = 0; j < zombiesSeleccionada.size(); j++) {
					if(zombiesSeleccionada.get(j).getNombre() == "corredor") {
						zombiesSeleccionada.remove(j);
						System.out.println("\n  <<ataque especial activa>>\n");
					}
				}
				return true;
			}
			else if(nombreArma == "daga") {
				jugadoresSeleccionada.get(i).getArma().ataqueEspecial();
				jugadoresSeleccionada.get(i).setAtaqueEspecial(true);
				return false;
			}
		}
		else {
			System.out.println("\n     ***No se puede ulitizar ataque especial mas de 1 cada nivel**\n");
		}
		return false;
	}
	
	// controla la inicialización de zombies con sus habilidades especiales, que se activan segun una probabilidad
	private void initZombie(int tipoDeZombie) {
		int randZombiHabilidad = rand.nextInt(101);
		if(randZombiHabilidad >= 95) {
			if(tipoDeZombie == 1) {
				System.out.println("   <<Habilidad corredor activa>> ");
				for(int j = 0; j < zombiesSeleccionada.size(); j++) {
					if(zombiesSeleccionada.get(j).getTipo() == 1) {
						zombiesSeleccionada.remove(j);
						j--;
					}
				}
			}
			else if(tipoDeZombie == 2) {
				System.out.println("   <<Habilidad caminante activa>> ");
				int countCaminante = 0;
				for(int j = 0; j < zombiesSeleccionada.size(); j++) {
					if(tipoDeZombie == 2) {
						countCaminante++;
					}
				}
				for(int j = 0; j < (countCaminante * 2); j++) {
					zombiesSeleccionada.add(allZombies.get(2));
				}
			}
			else if(tipoDeZombie == 3) {
				System.out.println("   <<Habilidad gord activa>> ");
				for(int j = 0; j < zombiesSeleccionada.size(); j++) {
					if(zombiesSeleccionada.get(j).getTipo() == 3) {
						zombiesSeleccionada.remove(j);
						j--;
					}
				}
			}
		}
	}
	
	// imprime por pantalla las estadisticas de un jugador en su turno
	private String turnoJugador(int i) {
		return jugadoresSeleccionada.get(i).toString();
	}
	
	private void generarZombie() {
		Gordo gordo = new Gordo();
		allZombies.add(gordo);
		Corredor corredor = new Corredor();
		allZombies.add(corredor);
		Caminante caminante = new Caminante();
		allZombies.add(caminante);
	}
	
	// Crea los zombies con los que se van a jugar en contra segun el nivel, de forma aleatoria entre los 3 tipos de zombies que existen
	private void zombieAleatorio() {
		System.out.print("\n         ==| ");
		for(int i = 0; i < nivel; i++) {
			int numeroAleatorio = rand.nextInt(3);
			zombiesSeleccionada.add(allZombies.get(numeroAleatorio));
			System.out.print(zombiesSeleccionada.get(i).getNombre() + " ");
		}
		System.out.println("|==");
	}
	
	// segun el resultado que devuelva getArma, se obtiene un arma determinada o no se obtiene nada
	private void buscar(int i) {
		
		Arma arma = Zombicide.getArma();
		if(arma == null) {
			System.out.println("Mala suerte!! no obtienes nada");
		}
		else {
			System.out.println("Obtienes el arma " + arma.getNombre());
			jugadoresSeleccionada.get(i).armaJugador.add(arma);
		}
	}
	
	// Imprime por pantalla la lista de armas que tiene un jugador y este puede elegir una
	private void cambiarArma(int i) {
		System.out.println("Elije tu arma: (Introduce número)");
		for(int j = 0 ; j < jugadoresSeleccionada.get(i).armaJugador.size(); j++) {
			System.out.println((j+1) + "- " + jugadoresSeleccionada.get(i).armaJugador.get(j).getNombre());
		}
		int eligirArma = scanner.nextInt();
		jugadoresSeleccionada.get(i).setArma(jugadoresSeleccionada.get(i).armaJugador.get(eligirArma - 1));
	}
		
}
