package humanoide;

public class Humanoide {

    private String nombre = "";
    public int salud = 0;
    private int saludMaxima = 0;

    public Humanoide(String nombre, int salud, int saludMaxima) {
    	setNombre(nombre);
    	setSalud(salud);
    	setSaludMaxima(saludMaxima);
    }
    
    
    public String getNombre() {
        return nombre;
    }

    private void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSalud() {
        return salud;
    }

    public void setSalud(int salud) {
        this.salud = salud;
    }

    public int getSaludMaxima() {
        return saludMaxima;
    }

    private void setSaludMaxima(int saludMaxima) {
        this.saludMaxima = saludMaxima;
    }
    
    // cura a los personajes que continuan vivos
    public void curar() {
    	if(salud < saludMaxima) {
    		salud++;
    	}
    }
  
    
}
