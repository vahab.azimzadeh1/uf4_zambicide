package zombie;

import humanoide.Humanoide;

public class Zombie extends Humanoide {

	private int movimiento;
	private int dano;
	private int tipo;
	
	public Zombie(String nombre, int salud, int saludMaxima, boolean vivo, int movimiento, int dano, int tipo) {
		super(nombre, salud, saludMaxima);
		setTipo(tipo);
		setMovimiento(movimiento);
		setDano(dano);
	}

	

	public int getTipo() {
		return tipo;
	}
	private void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getMovimiento() {
		return movimiento;
	}
	private void setMovimiento(int movimiento) {
		this.movimiento = movimiento;
	}

	public int getDano() {
		return dano;
	}
	private void setDano(int dano) {
		this.dano = dano;
	}
}
