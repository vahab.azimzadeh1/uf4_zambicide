package arma;

public class Espada extends Arma {

	final private String nombre = "espada";
	final private int dano = 1;
	final private int alcance = 1;
	final private int acierto = 4;

	public Espada() {
		
	}
	
	
//	public String ataqueEspecial() {
//		return "";
//	}

	public String getNombre() {
		return nombre;
	}

	public int getDano() {
		return dano;
	}

	public int getAlcance() {
		return alcance;
	}

	public int getAcierto() {
		return acierto;
	}
	
}
