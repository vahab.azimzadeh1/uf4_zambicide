package arma;

public class Arma {
	final private String nombre = "daga";
	final private int dano = 1;
	final private int alcance = 1;
	final private int acierto = 4;
	
	public Arma() {
		
	}
	
	
	public String getNombre() {
		return nombre;
	}

	public int getDano() {
		return dano;
	}

	public int getAlcance() {
		return alcance;
	}

	public int getAcierto() {
		return acierto;
	}
	
//	ataque especial de clase arma no va a hacer nada
	public void ataqueEspecial() {
		System.out.println("\n   <<No hay habilidad especial>>\n");
	}
	

	
}
