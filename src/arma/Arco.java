package arma;

public class Arco extends Arma {

	final private String nombre = "arco";
	final private int dano = 1;
	final private int alcance = 2;
	final private int acierto = 3;
	
	public Arco() {
		
	}
	
//	public String ataqueEspecial() {
//		return "";
//	}

	public String getNombre() {
		return nombre;
	}

	public int getDano() {
		return dano;
	}

	public int getAlcance() {
		return alcance;
	}

	public int getAcierto() {
		return acierto;
	}
	
}
