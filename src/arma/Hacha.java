package arma;


public class Hacha extends Arma {

	final private String nombre = "hacha";
	final private int dano = 2;
	final private int alcance = 1;
	final private int acierto = 3;
	
	public Hacha() {
		
	}


	public String getNombre() {
		return nombre;
	}

	public int getDano() {
		return dano;
	}

	public int getAlcance() {
		return alcance;
	}

	public int getAcierto() {
		return acierto;
	}
	
}
