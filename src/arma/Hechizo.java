package arma;

public class Hechizo extends Arma {

	final private String nombre = "hechizo";
	final private int dano = 1;
	final private int alcance = 3;
	final private int acierto = 4;
	
	public Hechizo() {
		
	}

//	public String ataqueEspecial() {
//		return "";
//	}

	public String getNombre() {
		return nombre;
	}

	public int getDano() {
		return dano;
	}

	public int getAlcance() {
		return alcance;
	}

	public int getAcierto() {
		return acierto;
	}
	
}
