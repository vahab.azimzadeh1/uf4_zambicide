package principal;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;

import arma.Arco;
import arma.Arma;
import arma.Espada;
import arma.Hacha;
import arma.Hechizo;
import humanoide.Jugador;
import humanoide.Partida;



public class Zombicide {
	
	private static ArrayList<Jugador> jugadoresCreados = new ArrayList<Jugador>();
	private static ArrayList<Jugador> jugadoresSeleccionada = new ArrayList<Jugador>();
	private static ArrayList<Jugador> jugadorBorada = new ArrayList<Jugador>();
	private static ArrayList<Arma> losArmas = new ArrayList<Arma>();
	private static Random rand = new Random();
	private static Scanner scanner = new Scanner(System.in);
	private static int welcome = 1;
	
	// muestra el menu del juego y según lo que se seleccione ejecuta una función
	public static void main(String[] args) {
		if(welcome == 1) {
			System.out.println("\n      ^^^^BIENVENIDO^^^^\n");
			initObjeto();
			initPersonaje();
			welcome--;
		}
        int opcion = -1;
        while (opcion != 0) {
            mostrarMenu();
            opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    nuevaPartida();
                    break;
                case 2:
                    nuevoPersonaje();
                    break;
                case 0:
                    System.out.println("\n     HASTA LUEGO (◠‿◠) ");
                    System.exit(0);
                default:
					System.out.println("\n    -----Opción no válida!!! Selecciona una opción valida-----\n");
                    break;
            }
        }
    }
	
	// inicializa las diferentes armas del juego
    private static void initObjeto() {
			Arco arco = new Arco();
			losArmas.add(arco);
			Espada espada = new Espada();
			losArmas.add(espada);
			Hechizo hechizo = new Hechizo();
			losArmas.add(hechizo);
			Hacha hecha = new Hacha();
			losArmas.add(hecha);
			Arma daga = new Arma();
			losArmas.add(daga);
	}
    
    // Devuelve un arma dependiendo de un numero random
    public static Arma getArma() {
    	int numeroRandom = rand.nextInt(6);
		if(numeroRandom == 5) {
			return null;
		}
		else {
			return losArmas.get(numeroRandom);
		}
    }
    
    // Inicilización de los 3 personajes por defecto
	private static void initPersonaje() {
			Jugador jugador1 = new Jugador("Max", 5, 5, losArmas.get(3));
			jugadoresCreados.add(jugador1);
			Jugador jugador2 = new Jugador("Jason", 5, 5, losArmas.get(2));
			jugadoresCreados.add(jugador2);
			Jugador jugador3 = new Jugador("Alex", 5, 5, losArmas.get(1));
			jugadoresCreados.add(jugador3);
	}

	private static void mostrarMenu() {
        System.out.println(""
        		+ " -----Seleccione una opción----- \n"
        		+ "   1- Nueva partida \n"
        		+ "   2- Nuevo personaje \n"
        		+ "   0- Salir");
    }
    
	//Gestiona la creación de una nueva partida, permitiendo al usuario seleccionar los jugadores, con ciertas restricciones
    private static void nuevaPartida() {
    	
    	System.out.print("Cuantos personajes jugarán?(Mínimo 3, Máximo 6) ==> ");
    	int numeroDePersonaje = scanner.nextInt();
    	
    	if(numeroDePersonaje == 3) {
    		System.out.println("\n     **Todos tus personajes han sido seleccionados** \n");
    		for(int i = 0; i < 3; i++) {
    			int randd = rand.nextInt(jugadoresCreados.size());
    			jugadoresSeleccionada.add(jugadoresCreados.get(randd));
    			jugadorBorada.add(jugadoresCreados.get(randd));
    			jugadoresCreados.remove(randd);
    		}
    		for(int i = 0; i < 3; i++) {
    			jugadoresCreados.add(jugadorBorada.get(i));
    		}
    		Partida nuevaPartida = new Partida(jugadoresSeleccionada, 3);
    	}
    	else if (numeroDePersonaje < 3) {
    		System.out.println("\n  **No se puede jugar con menos de 3 personajes**\n");
    		return;
    	}
    	else {
    		if(numeroDePersonaje > 6) {
    			System.out.println("\n  **No se puede jugar con más de 6 personajes**\n");
        		return;
    		}
    		if(numeroDePersonaje > jugadoresCreados.size()) {
    			System.out.println("\n   ***No hay suficientes personajes, primero crea más personajes.***\n");
    			return;
    		}
    		int count = 1;
    		for(Jugador nombres : jugadoresCreados) {
    			System.out.println(count + "- " + nombres.getNombre());
    			count++;
    		}
    		for(int i = 0; i < numeroDePersonaje; i++) {
    			System.out.print("Jugador " + (i+1) +" selecciona tu personaje:(Introduce número) ");
    			int suPersonaje = scanner.nextInt();
    			jugadoresSeleccionada.add(jugadoresCreados.get(suPersonaje - 1));
    		}
    		Partida nuevaPartida = new Partida(jugadoresSeleccionada, numeroDePersonaje);
    	}
    }

    // Creacion de personajes nuevos con la daga por defecto, no se permite crear más de 10 personaj
    private static void nuevoPersonaje() {
    	
    	if(jugadoresCreados.size() < 10) {
    		System.out.print("Intrudoce un nombre: ");
    		String nombreJugador = scanner.next();
    		Jugador jugadores = new Jugador(nombreJugador, 5, 5, losArmas.get(4));
    		jugadoresCreados.add(jugadores);
    		System.out.println();
    	}
    	else {
    		System.out.println("\n****No se puede tener más de 10 personajes****\n");
    	}
    }
    
    // limpia las siguientes arraylists para que no hayan problemas al iniciar una nueva partida tras finalizar otra
    public static void limpiezaJugadores() {
    	jugadoresSeleccionada.clear();
    	main(null);
    }
}